# Consensus Clustering

This folder contains the data and the files in order to replicate the results contained in the article "Consensus clustering methodology to improve molecular stratification of Non-Small Cell Lung Cancer" (TCGA patients only). 

## Getting started

The following R Packages will beinstalled after the execution of these files: 
- cluster
- foreach
- doSNOW
- parallel
- kohonen
- Spectrum
- iClusterPlus
- SNFtool
- survival
- survminer
- dplyr
- tidyr
- pracma

If the loading/installation of these packages shows some problems, the code will not run. 

## Files

The directory contains the following files: 
- functions.R: function file to be sourced 
- main.R: main file to be executed 
- data.rds: data file containing information about TCGA patients and genetic expression

## Usage

Run

## Additional functionalities

A tool for predicting labels of new samples has been developed and made publicly available at https://gitlab.com/deflect-public/consensus-clustering-predict together with a brief documentation of its usage.

## Authors and acknowledgment

Work has been performed by aizoOn Technology consulting as part of project DEFLeCT (Digital tEChnology For Lung Cancer Treatment).

<img src="https://aizoongroup.com/Style%20Library/images/logo.png" width="350">

## License

This work is licensed under the Gnu Affero Public License V3.0 (see https://gitlab.com/deflect-public/consensus-clustering/-/blob/main/LICENSE)

<img src="https://www.regione.piemonte.it/loghi/im/grafica/piede_fesr.jpg">
