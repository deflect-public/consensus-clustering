\documentclass{article}
\title{Subtyping Documentation}
\author{Manganaro, L., Cipollini F., Sabbatini, G.}
\usepackage{Sweave}
\begin{document}
\input{documentation-concordance}
\maketitle
\section{Overview} 

We implemented an ensemble learning model based on consensus clustering applied to log2-scaled gene expression data. The method is deployed in two phases: 
\begin{enumerate}
\item different clustering methods are run independently on the same data and the resulting cluster associations are obtained for each patient and method; 
\item clustering associations are compared and a voting process is established, in order to detect the most supported label for each patient. 
\end{enumerate}
In the following, these phases will be referred as \textbf{Phase 1} and \textbf{Phase 2}, respectively. 
These phases are called in sequence inside the main file on \emph{data.rds} data.

For TCGA data, an additional step of pre-processing is needed to convert and normalize the TCGA data to be compatible with DEFLeCT data. Such step is described in section \ref{preproc} together with the other pre-processing operations.

\section{Data preprocessing}\label{preproc}
This section describes the steps to obtain a compatible \texttt{X} and \texttt{patients} objects to be used with the methods contained in \emph{functions.R} file.

Some pre-processing steps are needed to make the data comparable with the ones adopted in the study. In the following we will consider to have: 
\begin{itemize}
\item a \texttt{X} R object containing gene expression data, with one row for each patient and a column for each gene; 
\item a \texttt{patients} object containing the clinical data of the patients, with one row for each patient and a column for each patient metadata.
\end{itemize}

For simplicy we suppose that \texttt{X} and \texttt{patients} matrices are all ordered by patient so that the gene expression gene in row 1 corresponds to the patients clinical data in row 1, for both TCGA and DEFLeCT datasets.

Given these data, in particular it is needed: 
\begin{enumerate}
\item To select the same set of genes of the DEFLeCT data (see file \textbf{\emph{TODO}} for reference):
\begin{Schunk}
\begin{Sinput}
> ## read Gene List dataset and filter
> genes = read.csv('TODO')
> X = X[,names(X)%in%genes]
\end{Sinput}
\end{Schunk}
\item To normalise the \texttt{X} data to TPM (transcripts per million), so that the values are less sensitive to differences in the clinical protocols of the studies adopting \emph{GRCh38\_p12\_genes.txt} file.
In order to do it, for TCGA dataset it was necessary to divide the counts of each gene by the gene length and then to normalize the sum of the values by patient multiplied by $10^6$, as shown in the following code:
\begin{Schunk}
\begin{Sinput}
> grch = read.csv('GRCh38_p12_genes.txt', sep = '\t')
> ## for each gene column compute gene length and normalize
> for(gene in names(X)){
+   Gene = grch[grch$Gene.name == gene,]
+   L = Gene$Gene.end..bp.[1] - Gene$Gene.start..bp.[1]
+   X[,gene] = X[,gene]/L
+ }
> ## for each patient row normalize by row
> X = (X/rowSums(X))*10^6
\end{Sinput}
\end{Schunk}
\item to remove genes with zero variance and to log2-transform the TPM values:
\begin{Schunk}
\begin{Sinput}
> ## eliminate features with 0 std
> to_keep = names(which(sapply(X,var) > 0))
> X = X[, names(X) %in% to_keep]
> ## compute log 2 per column  
> X = sapply(X, function(x)log2(x+1))
\end{Sinput}
\end{Schunk}
\end{enumerate}

\section{Phase 1: Consensus Clustering}
In our study, the ML clustering methods employed were: 
\begin{itemize}
\item K-Means clustering;
\item spectral clustering;
\item hierarchical clustering;
\item iClusterPlus;
\item SNF;
\end{itemize}
For the first three models, also a SOM approach was taken into account, before performing the clustering. 
Each method was employed with three different seeds, in order for it to be tested with three different initialization points.  
Moreover, four maximum number of clusters were employed.
Functions employing these methods are reported in \emph{functions.R} file.

The main method employed in Phase 1 is \texttt{EnsembleClustering} which takes as input: 
\begin{itemize}
\item Gene expression data (\texttt{data} field);
\item patients metadata (\texttt{patients} field);
\item clustering method name (\texttt{method} field);
\item the random generator seed (\texttt{seed} field);
\item number of clusters to consider (\texttt{nclust} field).
\end{itemize}
Internally this method performs the specified method selected and returns a list containing the updated patients metadata with the cluster label (\texttt{patient} object), the model employed (\texttt{model} object), and, if present, the SOM model (\texttt{som model} object).
This method is supposed to be called in parallel providing different method names among the ones reported in \emph{main.R} file.
\begin{Schunk}
\begin{Sinput}
> # PHASE 1
> source('functions.r')
> ## set parameters to run experiments
> cores = 4
> seeds = c(11,13,17)
> methods = c('SOM+KMeans',
+             'SOM+Hyerarchical', 
+             'SOM+Spectral', 
+             'KMeans',
+             'Hyerarchical', 
+             'Spectral',
+             'iCluster', 
+             'SNF')
> combinations = expand.grid(methods, seeds)
> ## run in parallel 
> cluster<-makeCluster(cores)
> registerDoSNOW(cluster)
> pb <- txtProgressBar(min = 1, max = nrow(combinations), style=3)
> progress <- function(n) setTxtProgressBar(pb, n)
> opts <- list(progress=progress)
> results = foreach(comb = 1:nrow(combinations), .options.snow=opts)%dopar%{
+   source('functions.r')
+   method = combinations$Var1[comb]
+   seed = combinations$Var2[comb]
+   return(EnsembleClustering(X, patients, method=method,
+                             seed = seed, nclust = nclust))
+ }
> stopCluster(cluster)
\end{Sinput}
\end{Schunk}

\section{Phase 2: Majority voting }
After the Phase 1 results are obtained, it is possible to perform the majority voting. 
Three methods are available in the \emph{functions.R} file:
\begin{itemize}
\item \texttt{conversion} function, to translate the different clustering labels to a common reference system;
\item \texttt{getmode} function, to compute mode;
\item \texttt{votingProcess} function, the function performing the voting.
\end{itemize}
The \textbf{votingProcess} function is the main function to be called as shown in \emph{main.R} file, requiring as input argument the results obtained in Phase 1 (\texttt{results} field) and the number of cluster (\texttt{nclust} field).
\begin{Schunk}
\begin{Sinput}
> # PHASE 2 
> source('functions.r')
> ## execute voting
> votingProcess(results, nclust)
\end{Sinput}
\end{Schunk}
In this method, firstly the results are aggregated in a single matrix, reporting all the different clustering methods result for each patient. 
For simplicity, we will consider \texttt{case\_id} as the identifier variable for each patient. 
\begin{Schunk}
\begin{Sinput}
> # create matrix of all results
> tempList = lapply(1:length(results),function(i){
+   x = results[[i]][[1]]
+   col = names(x)[grepl("cluster",names(x))]
+   if(length(col)==0){
+     j = which(names(x)=='init')
+     names(x)[j:ncol(x)] =  paste0("cluster",1:(ncol(x)-j+1))
+     col = names(x)[grepl("cluster",names(x))]
+   }
+   x = x[,c('case_id',col)]
+   names(x)[2:ncol(x)] =  paste(combinations[i,]$Var1, 
+                                combinations[i,]$Var2,col,sep="_")
+   x$case_id = as.character(x$case_id)
+   return(x)
+ })
> patMat = Reduce(function(x,y)dplyr::full_join(x,y,by = "case_id"), tempList)
\end{Sinput}
\end{Schunk}
Then some cleaning operations are performed, in particular methods which do not separate correctly LUAD and LUSC affected patients are deleted and not taken into consideration during the voting.
Also methods unable to classify some points and giving a NA prediction are not considered.
Then a reference model is selected in order to apply the conversion to report all the different cluster labels into a common reference system (from [1,2,3,4] to [A,B,C,D]). 
For this operation the reference methods labels are directly attributed to [A,B,C,D] labels without further processing (e.g. 1 ->  A, 2  -> B), while for all the other methods, the mode operation is used in order to attribute each old cluster to the most probable and similar cluster in terms of common patients. 
\begin{Schunk}
\begin{Sinput}
> # The selected method is iCluster_11_cluster5
> reference = patMat[,"iCluster_11_cluster5"]
> # translation with respect to the reference
> translations = list()
> classes = c("A","B","C","D","E","F")[1:nclust]
> for(col in names(patMat)[2:ncol(patMat)]){
+   x = patMat[,col]
+   order = unlist(lapply(1:nclust,function(i){getmode(reference[x == i])} ))
+   temp = (lapply(x, function(x)conversion(x,order,classes)))
+   if(any(unlist(lapply(temp,function(i)i==character(0))))){
+     temp[which(unlist(lapply(temp,function(i)i==character(0))))] <- NA
+   }
+   patMat[,col]  = unlist(temp)
+   translations[[col]] = order
+ }
\end{Sinput}
\end{Schunk}
After all the clustering methods have been correctly normalized to the same nomenclature, the final voting is performed by considering the most frequently voted class for each patient.
\begin{Schunk}
\begin{Sinput}
> #compute final vote removing methods which have too many NAs
> patMat = patMat[,which(sapply(patMat,function(x)sum(is.na(x))/length(x))<0.1)]
> patMat$VOTE = apply(patMat[,2:ncol(patMat)],1,getmode)
\end{Sinput}
\end{Schunk}
\section{Bonus: Data preprocessing to obtain \emph{data.rds} file}\label{preproc}
This section describes the steps to switch from the TCGA and DEFLeCT gene expression matrices to the \emph{data.rds} file.

The TCGA matrix can be downloaded from the git respository of the consensus clustering project (link), while the DEFLeCT matrix can be downloaded from the repository (GEA? \textbf{TODO}) at the link (link \textbf{TODO}).
These data will be respectively called in the following:
\begin{itemize}
\item a \texttt{X\_TCGA} R object containing TCGA gene expression data, with one row for each patient and a column for each gene plus a column (supposedly called "case\_id") with the patient identifier; 
\item a \texttt{X\_deflect} R object containing DEFLeCT gene expression data, with one row for each patient and a column for each gene plus a column (supposedly called "case\_id") with the patient identifier; 
\item a \texttt{patients\_TCGA} object containing the clinical data of the TCGA patients, with one row for each patient and a column for each patient metadata plus the same identifier column (supposedly called \texttt{case\_id}); 
\item a \texttt{patients\_deflect} object containing the clinical data of the DEFLeCT patients, with one row for each patient and a column for each patient metadata plus the same identifier column (supposedly called \texttt{case\_id}); 
\end{itemize}

For simplicy we suppose that \texttt{X} and \texttt{patients} matrices are all ordered by \texttt{case\_id} so that the gene expression gene in row 1 corresponds to the patients clinical data in row 1, for both TCGA and DEFLeCT datasets.

Some pre-processing steps are needed to make the data from the two studies comparable, in particular it is needed: 
\begin{enumerate}
\item To normalise the TCGA data to TPM (transcripts per million) using the same set of genes of the DEFLeCT data, so that the values are less sensitive to differences in the clinical protocols of the studies (the DEFLeCT data are already provided in TPM).
In order to do it, for TCGA dataset it is necessary to divide the counts of each gene by the gene length and then to normalize the sum of the values by patient multiplied by $10^6$, as shown in the following code:
\begin{Schunk}
\begin{Sinput}
> ## read TCGA dataset and Gene matrix 
> grch = read.csv('GRCh38_p12_genes.txt', sep = '\t')
> ## for each gene column compute gene length and normalize
> for(gene in names(X_TCGA[,-grep("case_id",colnames(X_TCGA))])){
+   Gene = grch[grch$Gene.name == gene,]
+   L = Gene$Gene.end..bp.[1] - Gene$Gene.start..bp.[1]
+   X_TCGA[,gene] = X_TCGA[,gene]/L
+ }
> ## for each patient row normalize by row
> X_TCGA[,-grep("case_id",colnames(X_TCGA))] = 
+   (X_TCGA[,-grep("case_id",colnames(X_TCGA))]/
+   rowSums(X_TCGA[,-grep("case_id",colnames(X_TCGA))]))*10^6
\end{Sinput}
\end{Schunk}
\item To keep only the common genes of the two datasets (TCGA/DEFLeCT). This can by achieved by simply keeping the common columns between the two matrices with the following code:
\begin{Schunk}
\begin{Sinput}
> ## Join of the two datasets for expression
> X = bind_rows(X_TCGA,X_deflect)
> X = X[ , apply(X, 2, function(x) !any(is.na(x)))]
> ## Join of the two datasets for patients data
> patients = bind_rows(patients_TCGA,patients_deflect)
\end{Sinput}
\end{Schunk}
\item to remove genes with zero variance and to log2-transform the TPM values:
\begin{Schunk}
\begin{Sinput}
> ## eliminate features with 0 std for TCGA
> to_keep = names(which(sapply(X[,-grep("case_id",colnames(X))],var) > 0))
> X = X[, names(X) %in% c('case_id',to_keep)]
> ## compute log 2 per column  
> X[,-grep("case_id",colnames(X))] = 
+   sapply(X[,-grep("case_id",colnames(X))],
+   function(x)log2(x+1))
\end{Sinput}
\end{Schunk}
\end{enumerate}
\end{document}
